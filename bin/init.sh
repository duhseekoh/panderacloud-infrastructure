#!/bin/bash

set -e

cd terraform
# rm -rf .terraform
terraform init \
  -input=false \
  -backend-config="region=us-east-1" \
  -backend-config="bucket=panderacloud-infra-us-east-1" \
  -backend-config="key=config/panderacloud/terraform.tfstate" \
  -backend-config="dynamodb_table=TerraformLock" \
  $*
