#!/bin/bash

set -e

if [ ! -d workspace ]; then
  mkdir workspace
fi

cd terraform
terraform plan \
  -input=false \
  -var-file="environments/prod.tfvars" \
  -out="../workspace/prod.tfplan"

