#!/bin/bash

set -e

cd terraform
terraform apply \
  -input=false \
  "../workspace/prod.tfplan"
