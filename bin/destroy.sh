#!/bin/bash

set -e

if [ -e local.env ]; then
  source local.env
fi

cd terraform
terraform destroy \
  -var-file="environments/prod.tfvars" \
  $@
