Meta
====

Before Terraform can manage your AWS resources, you will first need to configure some things in AWS. This folder enables bootstrapping your AWS environment for use with Terraform. The Terraform script will:

- ~Create a private, versioned S3 bucket for storing the Terraform state for all environments.~
  - We performed this step manually for this project.
- ~Create a DynamoDB table Terraform will use to lock state files.~
  - We performed this step manually for this project.
- Create a "terraform" IAM group with a pretty generous policy in order to allow Terraform to make modifications to your AWS infrastructure.

The script does **not**:

- Create an IAM user for CI and add the user to the "terraform" group.

## Usage

To bootstrap (or modify) the meta-infrastructure, run the following commands from a user with sufficient AWS privileges.

```
terraform plan -out="meta.tfplan"
```

Review the output for anything unexpected, like deleting resources you don't want deleted. If you are satisfied with the changes, run:

```
terraform apply "meta.tfplan"
```
