provider "aws" {
  version = "~> 0.1"
  region  = "${var.aws_region}"
}

resource "aws_iam_group" "terraform" {
  name = "terraform"
}

resource "aws_iam_policy" "terraform" {
  name        = "Terraform"
  description = "Allow Terraform to administer AWS resources"
  policy      = "${file("TerraformPolicy.json")}"
}

resource "aws_iam_group_policy_attachment" "terraform_policy" {
  group      = "${aws_iam_group.terraform.name}"
  policy_arn = "${aws_iam_policy.terraform.arn}"
}
