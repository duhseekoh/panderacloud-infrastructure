variable "aws_region" {
  type    = "string"
  default = "us-west-2"
}

variable "stack_name" {
  type        = "string"
  description = "the name of your stack"
  default = "panderacloud"
}
