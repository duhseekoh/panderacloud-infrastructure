panderacloud-sonarqube-infrastructure
=================

This project defines infrastructure for AWS using Terraform for SonarQube for Pandera Cloud.

Features:

- State is stored separately for each environment. (See [this](https://charity.wtf/2016/03/30/terraform-vpc-and-why-you-want-a-tfstate-file-per-env/) for why.) Scripts are provided to help manage swapping between said states.
- CircleCI configuration for deploying develop to dev, release/* to staging, and master to prod.
- Initial skeleton to set up a VPC with some reasonable defaults.

## Setup

See [./meta/README.md]() for information about configuring your AWS environment to work with this project.

## Usage

The `bin` folder provides scripts to correctly apply the Terraform scripts. You **should not** use the `terraform` command directly!

The following environment variables are used by these scripts:

- `ENV` - The environment to deploy, such as `dev` or `staging`.
- `TF_CONFIG_REGION` - The AWS region that hosts your Terraform state.
- `TF_CONFIG_BUCKET` - The name of the AWS bucket that hosts your Terraform state.


