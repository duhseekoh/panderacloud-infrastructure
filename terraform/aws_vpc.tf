module "panderacloud_vpc" {
  source = "git@github.com:panderasystems/terraform-modules.git//aws_vpc"

  vpc_name                  = "panderacloud-${var.env}"
  cidr_block                = "10.0.0.0/16"
  private_subnets           = ["10.0.0.0/20", "10.0.16.0/20"]
  public_subnets            = ["10.0.255.0/26", "10.0.255.64/26"]
  subnet_availability_zones = ["us-west-2a", "us-west-2b"]
}
