data "local_file" "panderacloud_public_key" {
  filename = "./files/panderacloud-${var.env}.pub"
}

resource "aws_key_pair" "panderacloud" {
  key_name   = "panderacloud-${var.env}"
  public_key = "${data.local_file.panderacloud_public_key.content}"
}

module "bastion_host" {
  source = "git@github.com:panderasystems/terraform-modules.git//aws_ec2_service"

  ami_id                     = "ami-218eed59"
  vpc_id                     = "${module.panderacloud_vpc.vpc_id}"
  service_name               = "panderacloud-${var.env}-bastion"
  subnet_type                = "public"
  instance_key_name          = "${aws_key_pair.panderacloud.key_name}"
  default_security_group_id  = "${module.panderacloud_vpc.vpc_default_security_group_id}"
  default_security_group_ids = "${module.panderacloud_vpc.default_security_group_ids}"
  subnet_ids                 = "${module.panderacloud_vpc.subnet_ids}"
}

# Grant SSH access into the bastion from the outside world.
resource "aws_security_group_rule" "world_to_bastion_ssh" {
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  security_group_id = "${module.bastion_host.security_group_id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

# Grant the bastion access to SSH
resource "aws_security_group_rule" "bastion_to_private" {
  type                     = "ingress"
  from_port                = "0"
  to_port                  = "65535"
  protocol                 = "tcp"
  security_group_id        = "${module.panderacloud_vpc.vpc_default_security_group_id_private}"
  source_security_group_id = "${module.bastion_host.security_group_id}"
}

# Grant the bastion access to Postgres
resource "aws_security_group_rule" "bastion_to_private_postgres" {
  type                     = "ingress"
  from_port                = "5432"
  to_port                  = "5432"
  protocol                 = "tcp"
  security_group_id        = "${module.panderacloud_vpc.vpc_default_security_group_id_private}"
  source_security_group_id = "${module.bastion_host.security_group_id}"
}
