resource "aws_security_group" "panderacloud_lb" {
  vpc_id = "${module.panderacloud_vpc.vpc_id}"

  tags = {
    Name      = "panderacloud-${var.env}-lb"
    terraform = "true"
  }
}

resource "aws_security_group_rule" "expose_http" {
  security_group_id = "${aws_security_group.panderacloud_lb.id}"
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"

  cidr_blocks = [
    "0.0.0.0/0",
  ]
}

resource "aws_security_group_rule" "expose_https" {
  security_group_id = "${aws_security_group.panderacloud_lb.id}"
  type              = "ingress"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"

  cidr_blocks = [
    "0.0.0.0/0",
  ]
}

resource "aws_lb" "panderacloud" {
  name     = "${var.env}-panderacloud"
  internal = false

  security_groups = [
    "${module.panderacloud_vpc.vpc_default_security_group_id}",
    "${module.panderacloud_vpc.vpc_default_security_group_id_public}",
    "${aws_security_group.panderacloud_lb.id}",
  ]

  subnets = [
    "${module.panderacloud_vpc.subnet_ids_public}",
  ]
}

resource "random_pet" "lb_target_group_http" {
  keepers = {
    vpc_id = "${module.panderacloud_vpc.vpc_id}"
  }
}

resource "aws_lb_target_group" "default" {
  name        = "${random_pet.lb_target_group_http.id}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "${random_pet.lb_target_group_http.keepers.vpc_id}"
  target_type = "ip"

  health_check {
    matcher = "200-499"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = "${aws_lb.panderacloud.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.default.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = "${aws_lb.panderacloud.arn}"
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${data.aws_acm_certificate.cert.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.default.arn}"
    type             = "forward"
  }
}

data "aws_acm_certificate" "cert" {
  domain = "*.${var.panderacloud_domain}"

  statuses = [
    "ISSUED",
  ]

  types = [
    "AMAZON_ISSUED",
  ]

  most_recent = true
}
