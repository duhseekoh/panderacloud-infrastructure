module "panderacloud_ecs" {
  source = "git@github.com:panderasystems/terraform-modules.git//aws_ecs_cluster"

  env    = "${var.env}"
  region = "${var.aws_region}"
  name   = "panderacloud"

  vpc_id            = "${module.panderacloud_vpc.vpc_id}"
  config_bucket     = "panderacloud-infra-us-west-2"
  config_key_prefix = "${var.env}/panderacloud/ecs/"

  instance_type        = "t2.medium"
  cluster_desired_size = 3
  cluster_min_size     = 1
  cluster_max_size     = 3

  instance_key_name = "${aws_key_pair.panderacloud.key_name}"

  instance_security_group_ids = [
    "${module.panderacloud_vpc.vpc_default_security_group_id}",
    "${module.panderacloud_vpc.vpc_default_security_group_id_private}",
  ]

  cluster_subnet_ids = ["${module.panderacloud_vpc.subnet_ids_private}"]
}
