variable "env" {
  default = "prod"
}

variable "aws_region" {
  default = "us-west-2"
}

variable "panderacloud_domain" {
  default = "panderacloud.com"
}
