terraform {
  backend "s3" {}
}

provider "aws" {
  version = "~> 1.7"
  region  = "${var.aws_region}"
}

provider "local" {
  version = "~> 1.1"
}

provider "random" {
  version = "~> 1.1"
}

provider "template" {
  version = "~> 1.0"
}
