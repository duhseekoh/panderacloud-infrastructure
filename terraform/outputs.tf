output "panderacloud_domain" {
  value = "${var.panderacloud_domain}"
}

output "vpc_id" {
  value = "${module.panderacloud_vpc.vpc_id}"
}

output "vpc_default_security_group_id" {
  value = "${module.panderacloud_vpc.vpc_default_security_group_id}"
}

output "vpc_default_security_group_id_private" {
  value = "${module.panderacloud_vpc.vpc_default_security_group_id_private}"
}

output "vpc_default_security_group_id_public" {
  value = "${module.panderacloud_vpc.vpc_default_security_group_id_public}"
}

output "vpc_subnet_ids_public" {
  value = ["${module.panderacloud_vpc.subnet_ids_public}"]
}

output "vpc_subnet_ids_private" {
  value = ["${module.panderacloud_vpc.subnet_ids_private}"]
}

output "lb_security_group" {
  value = "${aws_security_group.panderacloud_lb.id}"
}

output "lb_listener_http" {
  value = "${aws_lb_listener.http.id}"
}

output "lb_listener_https" {
  value = "${aws_lb_listener.https.id}"
}

output "lb_dns_name" {
  value = "${aws_lb.panderacloud.dns_name}"
}

output "ecs_cluster_id" {
  value = "${module.panderacloud_ecs.cluster_id}"
}

output "acm_certificate_arn" {
  value = "${data.aws_acm_certificate.cert.arn}"
}
